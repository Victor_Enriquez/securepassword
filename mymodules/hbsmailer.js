var nodemailer = require('nodemailer');
var hbs = require('nodemailer-express-handlebars');
var options = {
 viewEngine: {
     extname: '.hbs',
     layoutsDir: 'views/email/'
 },
 viewPath: 'views/email/',
 extName: '.hbs'
};

var gmail = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'proyectoFinalDisenio@gmail.com',
        pass: 'proyectofinal'
    }
});

gmail.use('compile', hbs(options));

module.exports = gmail;
