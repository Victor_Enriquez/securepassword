
var express = require('express');
var router = express.Router();


router.get('/', function(req, res, next) {
  console.log('GET index');
res.render('index',{title:'Equipo 5',session:req.session.user});
});

router.get('/about', function(req, res, next) {
res.render('about',{title:'Equipo 5',session:req.session.user});
});

router.get('/thankyou', function(req, res, next) {
res.render('thankyou',{title:'Equipo 5',session:req.session.user});
});

router.get('/token', function(req, res, next) {
res.render('token',{title:'token'});
});

router.get('/welcome', function(req, res, next) {
res.render('welcome',{title:'token',session:req.session.user});
});



module.exports = router;
