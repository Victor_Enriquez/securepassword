var crypto = require('crypto');
var express = require('express');
var router = express.Router();

var tablaUsers = require('../models/users');
var hbsmailer = require("../mymodules/hbsmailer");

router.get('/', function(req, res, next) {
  tablaUsers.findAll().then(function(users) {
    res.json(users);
  });
});

router.get('/create', function(req, res, next) {
res.render('create',{title:'Equipo 5',session:req.session.user});
});

router.post('/createUser', function(req, res, next) {
  var secret = '1392542397501e1158418adae09d694ffb8ed833a3a5e8a017e15ba565d28c70';
  var pass =req.body.password;
  var hash = crypto.createHmac('sha256', secret).update(pass).digest('hex');
//---Entries with values
if (req.body.email != '' && req.body.password != '') {
  //----Search repeated
  tablaUsers.findOne({where:{email:req.body.email}}).then(function(users) {
    if (users) {
      //----If repeated send message, already exist
      res.render('errorValues',{error:'User already exist, use an other email' , page:'/users/create'});
    }else{
      //----If not repeated, create new user
        var newUserData = {
            "email":req.body.email,
            "name":req.body.name,
            "password":hash,
            "address":req.body.address,
            "domainpassword":{},
            "status":"active"
        };
        //----Insert new user on the DB
        tablaUsers.build(newUserData).save().then(function(new_user){
              console.log("Se Creo usuario ");
              //---Send email to confirmation
              hbsmailer.sendMail({
                         from: 'proyectoFinalDisenio@gmail.com',
                         to: req.body.email,
                         subject: 'Welcome ' + req.body.name,
                         template: 'welcome',
                         context: {
                              name : req.body.name,
                              confirm_link : 'http://securepassword.hopto.org/users/confirm_email/' + new_user.id
                         }
                     }, function (error, response) {
                        console.log(error);
                        console.log(response);
                        console.log('mail sent');
                         hbsmailer.close();
                     });
              res.redirect("/token");
          });
    }
  });
}else{ //---end if with entries values
res.render('errorValues',{error:'Invalid input, maybe undefined values' , page:'/users/create'});
}
});

router.get('/confirm_email/:id', function(req, res, next) {
  var query = {id:req.params.id};
  tablaUsers.findOne( { where: query } ).then(function(user)
  {
    if(user){
      req.session.user=user;
      res.redirect('/token/welcome');
    }else{
      res.render('errorValues',{error:'Login failed' , page:'/login'});
    }
  });
});

router.delete('/deleteUser/:id', function(req, res, next) {
var id=req.params.id;
	tablaUsers.destroy({where:{id:id}});
});


module.exports = router;
