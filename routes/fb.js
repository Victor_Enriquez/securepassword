var express = require('express');
var router = express.Router();
var rest = require('../mymodules/rest');
var User = require('../models/users');

router.get('/',function(req,res,next){
	res.render('signin',{title:'Equipo 5',session:req.session.user});
});

router.post('/loginFB',function(req,res,next){
  var redirect_uri = "http://securepassword.hopto.org:80/fb/facebook_redirect"
  res.redirect("https://www.facebook.com/v2.10/dialog/oauth?client_id=1136026253195221&redirect_uri="+redirect_uri);
});

router.get('/facebook_redirect',function(req,res,next){
  //capturar datos de facebook
	var existe=0;
  	var redirect_uri = "http://securepassword.hopto.org:80/fb/facebook_redirect";
  	var facebook_code = req.query.code;
  	var accesstoken_call = {
  	    host: 'graph.facebook.com',
  	    port: 443,
  	    path: '/v2.10/oauth/access_token?client_id=1136026253195221&redirect_uri='+redirect_uri+
  	    '&client_secret=926add611effd76906a1fb2375ebc75e&code='+facebook_code,
  	    method: 'GET',
  	    headers: {'Content-Type': 'application/json'}
  	};

  	rest.getJSON(accesstoken_call, function(statusCode, access_token_response) {
  	    var FB_path = '/me?fields=id,name,email,picture&access_token='+access_token_response.access_token;
  	    console.log("token: " + access_token_response.access_token);
  	    console.log("FB_path: " + FB_path);
  	    var userinfo_call = {
  		    host: 'graph.facebook.com',
  		    port: 443,
  		    path: FB_path,
  		    method: 'GET',
  		    headers: {'Content-Type': 'application/json'}
  		};
  		rest.getJSON(userinfo_call, function(statusCode, user_info_response) {
  			User.find( {where: { name : user_info_response.name  }} ).then(function(user){

		   	var newUser = {
         "email":user_info_response.email ,
         "name":user_info_response.name ,
         "password":access_token_response.access_token,
         "address":"",
				 "domainpassword":{},
         "status":"active"
     	};
				if (user) {
					req.session.user=user;
					console.log("Already exist, just login -----"+user);
					existe=1;
					res.redirect("/wallet");
				}else{
					User.build(newUser).save().then(function(user){
					 console.log("Add user b-cuz doesnt exist");
					 req.session.user=user;
					 res.redirect("/wallet");
						});
				}//---End else
  		});
  	});
  	});
});

module.exports = router;
