
var express = require('express');
var router = express.Router();
var Handlebars=require('hbs');
var crypto = require('crypto'),
    algorithm = 'aes-256-ctr',
    password = '8bef6dc6be5d6651557e11cfe3f9c37c79f813cc5c1d463f9464b10c1fd5d31c';
var tablaUsers = require('../models/users');
var randomize = require('randomatic');

router.get('/', function(req, res, next) {
  var flag=false;
  if (req.session.user) {
    var id = req.session.user.id;
    tablaUsers.findOne({where:{id:id}}).then(function(users) {
          var name = users.name;
          var passwd = users.domainpassword;
          var keys = Object.keys(passwd);

          //----Decrypt all password to show
          var result = [];
          var cont=0;
          for (var pass in passwd) {
            result.push({dom:keys[cont],pass:decrypt(passwd[pass])});
            cont++;
          }

          if (cont==0) {
            flag=true;
          }

          res.render('wallet',{name:name , domain:keys, pass:result,session:req.session.user,flag:flag});
        });
    }else{
        res.redirect('/login');
    }
});

router.post('/addPass', function(req, res, next) {
  var domain=req.body.domain;

var flag=1;
if (req.body.domain != '' ) {

      if (req.session.user){
        req.session.user.password=randomize('Aa0',32);
          var id=req.session.user.id;
          tablaUsers.findOne({where:{id:id}}).then(function(users) {
            var keys = Object.keys(users.domainpassword);
                    for (var key in keys) {
                          if (keys[key] != domain) {
                            flag=1;
                          }else{
                            flag=0;
                            break;
                          }
                    }
                    if (flag != 0) {
                      users.domainpassword[domain] =  encrypt(req.session.user.password);
                      var result =users.domainpassword;
                      tablaUsers.update({domainpassword: result},{where:{id:id}});
                      res.redirect("/wallet/editWallet");
                    }else{
                      res.render('errorValues',{error:'Repeat domain' , page:'/wallet/editWallet'});
                    }

              });
          }else{
            res.render('errorValues',{error:'Please login first' , page:'/login'});

          }
}else{
  res.render('errorValues',{error:'Please insert correct values, maybe undefined value' , page:'/wallet/editWallet'});
}
});

router.get('/getPass/:id', function(req, res, next) {

    tablaUsers.findOne({where:{id:req.params.id}}).then(function(users) {
      var passwd = users.domainpassword;
      var keys = Object.keys(passwd);
  //----Decrypt all password to show
    var result = [];
        for (var pass in passwd) {
              result.push(pass,decrypt(passwd[pass]));
            }
          res.send(result);
      });
});

router.post('/delPass/:id', function(req, res, next) {
  var domain=req.params.id;
if (req.session.user){
        var id=req.session.user.id;
        tablaUsers.findOne({where:{id:id}}).then(function(users) {
          var passwd = users.domainpassword;
          var json={};

              for (var pass in passwd) {
                    if (pass != domain) {
                        json[pass] = passwd[pass];
                    }
                  }

            delete users.domainpassword;
            tablaUsers.update({domainpassword: json},{where:{id:id}});
            res.redirect("/wallet/editWallet");
        });
  }else{
    res.render('errorValues',{error:'Please login' , page:'/wallet/editWallet'});
  }
});

router.get('/editWallet', function(req, res, next) {

  var hasDomain=false;
  if (req.session.user) {
    var id = req.session.user.id;
    tablaUsers.findOne({where:{id:id}}).then(function(users) {
          var name = users.name;
          var passwd = users.domainpassword;
          var keys = Object.keys(passwd);

          //----Decrypt all password to show
          var result = [];
          var cont=0;
          for (var pass in passwd) {
            result.push({dom:keys[cont],pass:decrypt(passwd[pass])});
            cont++;
          }
          if (cont==0) {
            hasDomain=true;
          }

          res.render('editWallet',{name:name , domain:keys, pass:result,session:req.session.user,flag:hasDomain});
        });
    }else{
        res.redirect('/login');
    }
});

function encrypt(text){
  var cipher = crypto.createCipher(algorithm,password)
  var crypted = cipher.update(text,'utf8','hex')
  crypted += cipher.final('hex');
  return crypted;
}

function decrypt(text){
  var decipher = crypto.createDecipher(algorithm,password)
  var dec = decipher.update(text,'hex','utf8')
  dec += decipher.final('utf8');
  return dec;
}

module.exports = router;
