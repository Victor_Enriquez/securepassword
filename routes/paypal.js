
var express = require('express');
var router = express.Router();
var paypal = require('paypal-rest-sdk');
var random = require('crypto-random-string');
var crypto = require('crypto');

var tablaUsers = require('../models/users');
var hbsmailer = require("../mymodules/hbsmailer");

router.get('/', function(req, res, next) {
  if (req.session.user) {
    res.render('paypal',{session:req.session.user});
  }else{
    res.redirect('/paypal/login');
  }
});

router.get('/login', function(req, res, next) {
    res.render('loginpaypal',{session:req.session.user});
});

var config = {
"api" : {
  "host" : "api.sandbox.paypal.com",
  "port" : "",
  "client_id" : "AW42ZFF3nR-4daWk5CK0zYdJgd5BjNYTFQaz1fF18SOrrP-vqxt0tw0mDUvsWvCrLir7bK1CCOLXY_6e",  // your paypal application client id
  "client_secret" : "EIYMixoX1iZw4VBRqttbisS3romLSjFdR6dvW2-kxJ6uiSSF_9k6Fp3Ji4V8w_4Lfh4mpbWmvHCEpwBa" // your paypal application secret id
}
}

paypal.configure(config.api);


router.get('/success', function(req, res) {
  var title= "Payment transfered successfully.";
  console.log('donwnload our plugin');
  tablaUsers.findOne({where:{email:req.session.user.email}}).then(function(users) {
    hbsmailer.sendMail({
               from: 'proyectoFinalDisenio@gmail.com',
               to: req.session.user.email,
               subject: 'Thank you ' + req.session.user.name,
               template: 'thanks',
               context: {
                    name : req.session.user.name,
                    confirm_link : 'http://securepassword.hopto.org/thankyou'
               }
           }, function (error, response) {
              console.log(error);
              console.log(response);
              console.log('mail sent');
               hbsmailer.close();
           });
  });
  res.render('thankyou',{session:req.session.user});
  });

// Page will display when you canceled the transaction
router.get('/cancel', function(req, res) {
  res.render('paypal',{title:'Payment canceled'});
});

router.post('/paynow', function(req, res) {
   // paypal payment configuration.
  var payment = {
  "intent": "sale",
  "payer": {
    "payment_method": "paypal"
  },
  "redirect_urls": {
    "return_url": 'http://securepassword.hopto.org/paypal'+"/success",
    "cancel_url": 'http://securepassword.hopto.org/paypal'+"/cancel"
  },
  "transactions": [{
    "amount": {
      "total":parseInt(req.body.amount),
      "currency":  req.body.currency
    },
    "description": req.body.description
  }]
};


  paypal.payment.create(payment, function (error, payment) {
  if (error) {
    console.log(error);
  } else {
    if(payment.payer.payment_method === 'paypal') {
      req.paymentId = payment.id;
      var redirectUrl;

      for(var i=0; i < payment.links.length; i++) {
        var link = payment.links[i];
        if (link.method === 'REDIRECT') {
          redirectUrl = link.href;
        }
      }
      res.redirect(redirectUrl);
    }
  }
});
});

router.post('/login', function(req, res, next) {
  var responselogin = {};
  var secret = '1392542397501e1158418adae09d694ffb8ed833a3a5e8a017e15ba565d28c70';
  var pass=req.body.password;
  var hash = crypto.createHmac('sha256', secret).update(pass).digest('hex');

  var query = {"email":req.body.email, "password":hash};

  tablaUsers.findOne( { where: query } ).then(function(user)
	{
		if(user){
      req.session.user=user;
      res.redirect('/paypal');
		}else{
      res.render('errorValues',{error:'Login failed', page:'/'});
		}
	});
});

module.exports = router;
