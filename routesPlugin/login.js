var crypto = require('crypto');
var express = require('express');
var router = express.Router();
var random = require('crypto-random-string');
var random = require('crypto-random-string');

var tablaUsers = require('../models/users');

router.get('/', function(req, res, next) {
res.render('signinPlugin',{title:''});
});
router.get('/pluginWallet', function(req, res, next) {
res.render('walletPlugin',{title:''});
});
router.get('/readme', function(req, res, next) {
res.render('readme',{title:''});
});

router.post('/', function(req, res, next) {
  var responselogin = {};
  var secret = '1392542397501e1158418adae09d694ffb8ed833a3a5e8a017e15ba565d28c70';
  var pass=req.body.password;
  var hash = crypto.createHmac('sha256', secret).update(pass).digest('hex');

  var query = {"email":req.body.email, "password":hash};

  tablaUsers.findOne( { where: query } ).then(function(user)
	{
		if(user){
      req.session.user=user;
      res.redirect('/walletPlugin');
		}else{
      res.send("Login failed");
		}
	});
});

router.get('/logout',function(req, res, next){
  req.session.destroy();
  console.log('logout');
  res.redirect('/walletPlugin');
});
module.exports = router;
