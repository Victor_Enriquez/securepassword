
var express = require('express');
var router = express.Router();
var Handlebars=require('hbs');
var crypto = require('crypto'),
    algorithm = 'aes-256-ctr',
    password = '8bef6dc6be5d6651557e11cfe3f9c37c79f813cc5c1d463f9464b10c1fd5d31c';
var tablaUsers = require('../models/users');
var random = require('crypto-random-string');

router.get('/', function(req, res, next) {

  if (req.session.user) {
    var id = req.session.user.id;
    tablaUsers.findOne({where:{id:id}}).then(function(users) {
          var name = users.name;
          var passwd = users.domainpassword;
          var keys = Object.keys(passwd);

          //----Decrypt all password to show
          var result = [];
          var cont=0;
          for (var pass in passwd) {
            result.push({dom:keys[cont],pass:decrypt(passwd[pass])});
            cont++;
          }

          res.render('walletPlugin',{name:name , domain:keys, pass:result});
        });
    }else{
        res.redirect('/signinPlugin');
    }
});


function encrypt(text){
  var cipher = crypto.createCipher(algorithm,password)
  var crypted = cipher.update(text,'utf8','hex')
  crypted += cipher.final('hex');
  return crypted;
}

function decrypt(text){
  var decipher = crypto.createDecipher(algorithm,password)
  var dec = decipher.update(text,'hex','utf8')
  dec += decipher.final('utf8');
  return dec;
}

module.exports = router;
