var express = require('express');
var sessions = require('express-session')
var expressHbs = require('express-handlebars');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var path = require('path');

var sigin = require('./routes/fb');
var login = require('./routes/login');
var users = require('./routes/users');
var wallet = require('./routes/wallet');
var index = require('./routes/index');
var paypal = require('./routes/paypal');
var contact = require('./routes/contact');
//---Plugin routes
var loginP = require('./routesPlugin/login');
var walletP = require('./routesPlugin/wallet');
var fbP = require('./routesPlugin/fbPlugin');

var app = express();

// view engine setup
app.engine('.hbs', expressHbs({defaultLayout: 'layout', extname: '.hbs'}));
app.set('view engine', 'hbs');

var allowCrossDomain = function(req, res, next) {
   res.header('Access-Control-Allow-Origin', '*');
   res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
   res.header('Access-Control-Allow-Headers', 'Content-Type');
   next();
}
app.use(allowCrossDomain);

app.use(express.static(path.join(__dirname, 'public')));

app.use(sessions({
  cookieName:'session',
  secret: '1392542397501e1158418adae09d694ffb8ed833a3a5e8a017e15ba565d28c70',
  duration: 21 * 60 * 60 * 1000,
  activeDuration: 1000 * 60 * 5
}));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/',index);
app.use('/fb',sigin);
app.use('/login', login);
app.use('/users', users);
app.use('/wallet',wallet);
app.use('/paypal',paypal);
app.use('/contact',contact);
//----plugin
app.use('/signinPlugin',loginP);
app.use('/walletPlugin',walletP);
app.use('/fbPlugin',fbP);

module.exports = app;
