var Sequelize = require('sequelize');

var mysql_host = process.env.MYSQLHOST;
if(mysql_host == undefined || mysql_host == ""){
    mysql_host = '13.59.16.0';
}
var mysql_pass = process.env.MYSQLPASS;
if(mysql_pass == undefined || mysql_host == ""){
    mysql_pass = 'root'; //--root for AWS
}

var sequelize = new Sequelize('wallet', 'root', mysql_pass, {
  host: mysql_host,
  dialect: 'mysql'
});

sequelize.Promise = global.Promise;

var User = sequelize.define('users', {
    "id": { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
    "email":{ type: Sequelize.STRING },
    "name": { type: Sequelize.STRING },
    "password" : { type: Sequelize.STRING },
    "domainpassword" : { type: Sequelize.JSON },
    "address" : { type: Sequelize.STRING },
    "created_at" : { type: Sequelize.DATE },
    "status" : { type: Sequelize.STRING,  defaultValue: Sequelize.NOW }
});
User.sync();
module.exports = User;
